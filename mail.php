<?php 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';

// Change Settings
$recipientMailAddress = 'sample@gmail.com';

$smtpHost = 'smtp.example.com';
$smtpMailAddress = 'sample@gmail.com';
$smtpMailFromName = 'Mail Sender Name';
$smtpPassword = '1234';

//Create an instance; passing `true` enables exceptions
$mail = new PHPMailer(true);

try {
    //Server settings
    $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host       = $smtpHost;                              //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = $smtpMailAddress;                       //SMTP username
    $mail->Password   = $smtpPassword;                          //SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
    $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

    //Recipients
    $mail->setFrom($smtpMailAddress, $smtpMailFromName);
    $mail->addAddress($recipientMailAddress);   //Add a recipient
    // $mail->addReplyTo($recipientMailAddress, 'Information');
    // $mail->addCC('cc@example.com');
    // $mail->addBCC('bcc@example.com');

    //Attachments
    // $mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name

    //Content
    $mail->isHTML(true);                                  //Set email format to HTML
    $mailData = generate_mail();
    print_r($mailData); // To Debug
    $mail->Subject = $mailData['subject'];
    $mail->Body    = $mailData['body'];
    // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}

function generate_mail() {
    $data = [];
    $data['subject'] = ' New Request From '.$_REQUEST['customer_name'];
    $data['body'] = '<h1>New Record</h1>
    <ul style="list-style:none">
        <li>Name:'.$_REQUEST['customer_name'].'</li>
        <li>Phone Number:'.$_REQUEST['phone_number'].'</li>
        <li>Mail Address:'.$_REQUEST['mail_address'].'</li>
    </ul>';
    return $data;
}